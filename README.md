# platformer

Unfinished experimental platformer (with level editor) from 2018,    
controlled with just one button and the mouse.  
Written in C++ using SDL2.

## build

to build use make  
requires g++, SDL2, and SDL2_Image  

## license
all original code licensed under MIT  

some of src/engine.cpp was was copied from an unknown tutorial  
therefore DO NOT assume that src/engine.cpp is under an open source license