#pragma once
#include "global.h"
#include <ctime>

namespace Game{

void change_platform_type(int d);
void grab();
void drop();
void update_mouse(float x, float y);
void save_current_level();
void next_level();
void last_level();
void new_level();
void new_platform();
void new_goal();
void new_cannon();
struct entity * init();
struct entity * update(int input);

}