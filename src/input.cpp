#include <SDL2/SDL.h>
#include "input.h"
#include "game.h"

static int input_game(){

	SDL_Event e;

	int quit = INPUT_NULL;

	while( SDL_PollEvent( &e ) != 0 ){

		if( e.type == SDL_QUIT ){

			return INPUT_QUIT;

		}
		else if( e.type == SDL_KEYDOWN ){

			switch (e.key.keysym.sym) {

			case SDLK_ESCAPE:
				return INPUT_QUIT;
				break;

			case SDLK_SPACE:
				return INPUT_BUTTON;
				break;
			case SDLK_UP:
				return INPUT_BUTTON;
			case SDLK_s:
				return INPUT_BUTTON;

			case SDLK_p:
				return INPUT_PAUSE;
				break;

			case SDLK_F1:
				Game::save_current_level();
				break;

			case SDLK_1:
				Game::last_level();
				break;

			case SDLK_2:
				Game::next_level();
				break;
			case SDLK_n:
				Game::new_level();
				break;
			case SDLK_e:
				Game::new_platform();
				break;
			case SDLK_g:
				Game::new_goal();
				break;
			case SDLK_c:
				Game::new_cannon();
				break;
			}
		}else if( e.type == SDL_KEYUP ){
			switch (e.key.keysym.sym) {
			case SDLK_o:
				Renderer::toggle_fullscreen();
				break;
			}
		}
		else if( e.type == SDL_MOUSEMOTION ){
			Game::update_mouse(e.motion.x,e.motion.y);
		}else if( e.type == SDL_MOUSEBUTTONDOWN){
			if(e.button.button == SDL_BUTTON_LEFT) Game::grab();
		}else if( e.type == SDL_MOUSEBUTTONUP){
			if(e.button.button == SDL_BUTTON_LEFT) Game::drop();
		}else if( e.type == SDL_MOUSEWHEEL){
			if(e.wheel.y > 0){
				Game::change_platform_type(1);
			}else if(e.wheel.y < 0){
				Game::change_platform_type(-1);
			}

		}
	}
	return INPUT_NULL;
}

namespace Input{
int input(int game_mode){

	switch(game_mode){
	case MODE_GAME:
		return input_game();

	}
	return 0;
}
}