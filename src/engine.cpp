#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <cmath>

#include "engine.h"
#include "options.h"
#include "game.h"


bool show_fps = false;


#define FRAME_VALUES 10
// An array to store frame times:
Uint32 frametimes[FRAME_VALUES];
// Last calculated SDL_GetTicks
Uint32 frametimelast;
// total frames rendered
Uint32 framecount;
// the value you want
float framespersecond;

SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
SDL_Texture* gTextures[TEX_TOTAL];


static SDL_Texture* load_texture( std::string path ){
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL ){
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else{
		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL ){
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	return newTexture;
}





static void fpsinit() {

        // Set all frame times to 0ms.
        memset(frametimes, 0, sizeof(frametimes));
        framecount = 0;
        framespersecond = 0;
        frametimelast = SDL_GetTicks();

}
static void fpsthink() {

        Uint32 frametimesindex;
        Uint32 getticks;
        Uint32 count;
        Uint32 i;

        // frametimesindex is the position in the array. It ranges from 0 to FRAME_VALUES.
        // This value rotates back to 0 after it hits FRAME_VALUES.
        frametimesindex = framecount % FRAME_VALUES;
        // store the current time
        getticks = SDL_GetTicks();
        // save the frame time value
        frametimes[frametimesindex] = getticks - frametimelast;
        // save the last frame time for the next fpsthink
        frametimelast = getticks;
        // increment the frame count
        framecount++;

        // Work out the current framerate
        // The code below could be moved into another function if you don't need the value every frame.
        // I've included a test to see if the whole array has been written to or not. This will stop
        // strange values on the first few (FRAME_VALUES) frames.
        if (framecount < FRAME_VALUES) {
            count = framecount;
        } else {
            count = FRAME_VALUES;
        }
        // add up all the values and divide to get the average frame time.
        framespersecond = 0;
        for (i = 0; i < count; i++) {
        	framespersecond += frametimes[i];
        }

        framespersecond /= count;
        // now to make it an actual frames per second value...
        framespersecond = 1000.f / framespersecond;

}




namespace Renderer{

void close(){
	//Destroy window
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	for(int i = 0; i<TEX_TOTAL; i++){
		SDL_DestroyTexture(gTextures[i]);
		gTextures[i] = NULL;
	}
	gWindow = NULL;
	gRenderer = NULL;
	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

void toggle_fullscreen(){
	options_fullscreen = !options_fullscreen;
	printf("%d\n",options_fullscreen );
}
bool init(std::string window_name){
	//init random seed
	srand(time(NULL));

	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ){
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( window_name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_RESIZABLE );
		if( gWindow == NULL ){
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL ){
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) ){
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	if(show_fps) fpsinit();

	//load texture
	//e.g gTextures[TEX_PLAYER] = load_texture("Assets/battle_rob.png");
	gTextures[TEX_PLAYER_LEFT] = load_texture("assets/textures/tex_player_left.png");
	gTextures[TEX_PLAYER_RIGHT] = load_texture("assets/textures/tex_player_right.png");
	gTextures[TEX_PLATFORM] = load_texture("assets/textures/tex_platform.png");
	gTextures[TEX_RED] = load_texture("assets/textures/tex_red.png");
	gTextures[TEX_BLUE] = load_texture("assets/textures/tex_blue.png");
	gTextures[TEX_PLAYER_LEFT_DIVE] = load_texture("assets/textures/tex_player_left_dive.png");
	gTextures[TEX_PLAYER_RIGHT_DIVE] = load_texture("assets/textures/tex_player_right_dive.png");
	gTextures[TEX_PLAYER_LEFT_SLIDE] = load_texture("assets/textures/tex_player_left_slide.png");
	gTextures[TEX_PLAYER_RIGHT_SLIDE] = load_texture("assets/textures/tex_player_right_slide.png");
	gTextures[TEX_PLAYER_RIGHT_SHEET] = load_texture("assets/textures/tex_player_right_sheet.png");
	gTextures[TEX_PLAYER_LEFT_SHEET] = load_texture("assets/textures/tex_player_left_sheet.png");

	gTextures[TEX_RESIZE] = load_texture("assets/textures/tex_resize.png");
	gTextures[TEX_PLATFORM_MOVE_X] = load_texture("assets/textures/tex_platform_move_x.png");
	gTextures[TEX_PLATFORM_MOVE_Y] = load_texture("assets/textures/tex_platform_move_y.png");
	gTextures[TEX_GOAL] = load_texture("assets/textures/tex_goal.png");
	gTextures[TEX_LAVA] = load_texture("assets/textures/tex_lava.png");

	gTextures[TEX_CANNON_BASE] = load_texture("assets/textures/tex_cannon_base.png");
	gTextures[TEX_CANNON_BARREL] = load_texture("assets/textures/tex_cannon_barrel.png");

	gTextures[TEX_BULLET] = load_texture("assets/textures/tex_bullet.png");
	//gTextures[TEX_BACKGROUND] = load_texture("assets/textures/tex_background.png");

	SDL_RenderSetLogicalSize(gRenderer, SCREEN_WIDTH, SCREEN_HEIGHT);

	return success;
}
void render_ents(struct entity *e){
	for(int i = 0; i < ents_max; i++){
		if(e[i].type != ENT_TYPE_NULL && !e[i].hide){
			
			SDL_Rect r = {(e[i].rect.x), e[i].rect.y, e[i].rect.w, e[i].rect.h};
			SDL_Rect f = e[i].frame_rect;

			SDL_SetRenderDrawColor(gRenderer,128,0,0,255);


			SDL_Point c = {e[i].rect.w/2,e[i].rect.h-8};
			if(!e[i].hide) SDL_RenderCopyEx(gRenderer,gTextures[e[i].texture],&f,&r, e[i].rotation, &c, SDL_FLIP_NONE);
		}
	}
}

void render_background(){

	SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
	SDL_RenderClear( gRenderer );
	SDL_Rect game_viewport = {0,0,SCREEN_WIDTH,SCREEN_HEIGHT};
	SDL_RenderSetViewport(gRenderer,&game_viewport);

	if(options_fullscreen) SDL_SetWindowFullscreen(gWindow,SDL_WINDOW_FULLSCREEN_DESKTOP);
	else SDL_SetWindowFullscreen(gWindow,0);

	SDL_Rect rb = {0,0,SCREEN_WIDTH,SCREEN_HEIGHT};

	//int t = TEX_BACKGROUND;

	//SDL_RenderCopy(gRenderer,gTextures[t],NULL,&rb);

	/*for(int i = 0; i<ents[0].total; i++){
		engine_render_ent(ents[i]);
	}*/
	/*SDL_RenderPresent(gRenderer);
	if(show_fps){
	    fpsthink();
	    printf("%f\n", framespersecond);
	}
	SDL_Delay(15);*/

}
void render_present(){

	SDL_RenderPresent(gRenderer);
	if(show_fps){
	    fpsthink();
	    printf("%f\n", framespersecond);
	}
	SDL_Delay(15);

}
}