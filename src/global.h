#pragma once
#include "SDL2/SDL.h"
#include <string>

const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;

const int WINDOW_WIDTH = 1440;
const int WINDOW_HEIGHT = 810;

const int LEVELS_MAX = 20;

enum {
	TEX_PLAYER_LEFT,
	TEX_PLAYER_RIGHT,
	TEX_PLATFORM,
	TEX_RED,
	TEX_BLUE,
	TEX_PLAYER_LEFT_DIVE,
	TEX_PLAYER_RIGHT_DIVE,
	TEX_PLAYER_LEFT_SLIDE,
	TEX_PLAYER_RIGHT_SLIDE,
	TEX_PLAYER_RIGHT_SHEET,
	TEX_PLAYER_LEFT_SHEET,
	TEX_RESIZE,
	TEX_PLATFORM_MOVE_X,
	TEX_PLATFORM_MOVE_Y,
	TEX_GOAL,
	TEX_LAVA,
	TEX_CANNON_BASE,
	TEX_CANNON_BARREL,
	TEX_BULLET,
	//TEX_BACKGROUND,
	TEX_TOTAL
};

enum{
	ENT_TYPE_PLAYER          = 0,
	ENT_TYPE_PLATFORM        = 1,
	ENT_TYPE_RESIZE          = 2,
	ENT_TYPE_PLATFORM_MOVE_X = 3,
	ENT_TYPE_PLATFORM_MOVE_Y = 4,
	ENT_TYPE_GOAL            = 5,
	ENT_TYPE_LAVA            = 6,
	ENT_TYPE_CANNON_BASE     = 7,
	ENT_TYPE_CANNON_BARREL   = 8,
	ENT_TYPE_BULLET          = 9,
	ENT_TYPE_NULL            = 10,
	ENT_TYPE_TOTAL           = 11
};



enum{
	PLAT_DEFAULT,
	PLAT_MOVE_X,
	PLAT_MOVE_Y,
	PLAT_TOTAL
};
struct entity{
	float x = 0;
	float y = 0;
	float velocity_x = 0.0f;
	float velocity_y = 0.0f;
	int type = ENT_TYPE_NULL;
	SDL_Rect rect = {0,0,0,0};
	SDL_Rect frame_rect = {0,0,0,0};
	int texture = 0;
	int total = 1;
	bool hide = false;
	int set_index = 0;
	float rotation = 0;
	int time = 0;
};

struct mouse_info{
	int x = 0.0f;
	int y = 0.0f;
	bool left = false;
	bool right = false;
};

const int level_ents_max = 20;
const int ents_max = 50;

struct level{
	std::string name = "";
	int ents_total = 0;
	struct entity entities[ents_max];

};




enum : int{
	INPUT_NULL,
	INPUT_QUIT,
	INPUT_BUTTON,
	INPUT_PAUSE
};


enum : int{
	MODE_GAME,
	MODE_MENU
};

