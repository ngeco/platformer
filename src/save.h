#pragma once
#include "global.h"
#include <string>


namespace SaveLoad{

void save_level(struct entity *ents, std::string name);
int load_level(struct entity *ents, std::string name);
int load_level_list(std::string *levels);
void save_level_list(std::string *levels, int levels_total);

}