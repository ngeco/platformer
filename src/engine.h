#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <cmath>
#include "global.h"


/*SDL_Texture* load_texture( std::string path );






void fpsinit();
void fpsthink();

void engine_render_ents(struct entity ent);*/


namespace Renderer{

	void render_background();
	void render_present();
	void render_ents(struct entity *ents);
	bool init(std::string window_name);
	void toggle_fullscreen();
	void close();

}