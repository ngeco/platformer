#pragma once

#include <SDL2/SDL.h>
#include "global.h"
#include "engine.h"


static int input_game();

namespace Input{

int input(int game_mode);

}