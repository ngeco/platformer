#include "save.h"
#include <fstream>

static void clear_ents(struct entity *ents){
	struct entity e;
	for(int i = 0; i < ents_max; i++){
		ents[i] = e;
	}
}


namespace SaveLoad{

void save_level(struct entity *ents, std::string name){
	std::ofstream f;
	std::string s = "assets/levels/" + name;
	f.open(s.c_str());

	
	f << ents[0].total << "\n";



	for(int i = 0; i < ents[0].total; i++){
		f<< (int)ents[i].x << " ";
		f<< (int)ents[i].y << " ";
		f<< ents[i].type << " ";

		f<< ents[i].rect.x << " ";
		f<< ents[i].rect.y << " ";
		f<< ents[i].rect.w << " ";
		f<< ents[i].rect.h << " ";

		f<< ents[i].frame_rect.x << " ";
		f<< ents[i].frame_rect.y << " ";
		f<< ents[i].frame_rect.w << " ";
		f<< ents[i].frame_rect.h << " ";

		f<< ents[i].texture << " ";

		f<< ents[i].set_index << " ";

		f<< ents[i].total << " \n";
	}

	f.close();
	
}

int load_level(struct entity *ents, std::string name){
	std::string s = "assets/levels/" + name;

	int resize = 0;

	std::ifstream f;
	f.open(s.c_str());
	
	if(f.good()){
		
		clear_ents(ents);

		f >> ents[0].total;

		for(int i = 0; i < ents[0].total; i++){
			struct entity e;
			f >> e.x;
			f >> e.y;
			f>> e.type;

			if(e.type == ENT_TYPE_RESIZE){
				resize = i;
			}

			f>> e.rect.x;
			f>> e.rect.y;
			f>> e.rect.w;
			f>> e.rect.h;

			f>> e.frame_rect.x ;
			f>> e.frame_rect.y ;
			f>> e.frame_rect.w;
			f>> e.frame_rect.h ;

			f>> e.texture;

			f>> e.set_index;
			
			f>> e.total;

			ents[i] = e;
		}

		return resize;
		//levels[current_level] = name;

	}else{
		printf("file not found\n");
	}



}
int load_level_list(std::string *levels){
	
	std::ifstream lvl_list;
	lvl_list.open("assets/levels/level_list");
	std::string s;

	std::getline(lvl_list,s);
	int levels_total = atoi(s.c_str());
	
	printf("%d\n",levels_total );

	for(int i = 0; i < levels_total; i++){

		lvl_list >> levels[i];
	}
	
	for(int i = 0; i < levels_total; i++){
		printf("%s\n",levels[i].c_str() );
	}

	return levels_total;
}
void save_level_list(std::string *levels, int levels_total){
	
	std::ofstream lvl_list;
	lvl_list.open("assets/levels/level_list");
	std::string s;

	lvl_list << levels_total << "\n";

	for(int i = 0; i < levels_total; i++){

		lvl_list << levels[i].c_str() << "\n";
	}

	printf("total : %d , current_level: %s\n", levels_total, levels[levels_total-1].c_str());

	for(int i = 0; i < levels_total; i++){
		printf("%s\n",levels[i].c_str() );
	}
	
}

}