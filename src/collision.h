#pragma once

#include <stdio.h>
#include "global.h"

enum : int{
	COLLISION_NULL,
	COLLISION_LEFT,
	COLLISION_RIGHT,
	COLLISION_TOP,
	COLLISION_BOTTOM,
	COLLISION_INSIDE
};

namespace Collision{

struct collision_rect{
	float x = 0.0f;
	float y = 0.0f;
	float w = 0.0f;
	float h = 0.0f;
};

struct collision{
	int dir = COLLISION_NULL;
	int ent = 0;
};

bool aabb_collision(int player, int collision, struct entity *ents);
struct collision aabb_collision_dir(int index, struct collision_rect p_0, struct collision_rect p_1, int collision, struct entity *ents);

}