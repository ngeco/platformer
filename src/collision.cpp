#include "collision.h"
#include "SDL2/SDL.h"

namespace Collision{

bool aabb_collsion(int player, int collision, struct entity *ents){
	for(int i = 1; i<ents_max; i++){
		SDL_Rect a = ents[0].rect;
		SDL_Rect b = ents[i].rect;

		if(	(a.x < b.x + b.w) && (a.x + a.w > b.x ) && (a.y < b.y + b.h) && (a.h + a.y > b.y)) {
			if(ents[i].type == collision) return true;
		}	
	}

	return false;
}

struct collision aabb_collision_dir(int index, struct collision_rect p_0, struct collision_rect p_1, int collision, struct entity *ents){
	struct collision c;
	for(int i = 0; i<ents_max; i++){
		struct collision_rect a = p_1;
		struct collision_rect b;

		b.x = ents[i].rect.x;
		b.y = ents[i].rect.y;
		b.w = ents[i].rect.w;
		b.h = ents[i].rect.h;

		float p_0_left = p_0.x;
		float p_0_right = p_0.x + p_0.w;
		float p_0_top = p_0.y;
		float p_0_bottom = p_0.y + p_0.h;

		float p_1_left = p_1.x;
		float p_1_right = p_1.x + p_1.w;
		float p_1_top = p_1.y;
		float p_1_bottom = p_1.y + p_1.h;

		float b_left = b.x;
		float b_right = b.x + b.w;
		float b_top = b.y;
		float b_bottom = b.y + b.h;

		if(	(p_1_left < b_right) && (p_1_right > b_left ) 
			&& (p_1_top < b_bottom) && (p_1_bottom > b_top)) {

			c.ent = i;

			//if(ents[i].type == collision){
			if(!ents[i].hide && i != index){

				c.dir = COLLISION_INSIDE;

				if(p_0_right < b_left && p_1_right >= b_left) c.dir = COLLISION_LEFT;
				if(p_0_left >= b_right && p_1_left < b_right) c.dir = COLLISION_RIGHT;
				if(p_0_bottom < b_top && p_1_bottom >= b_top) c.dir = COLLISION_TOP;
				if(p_0_top >= b_bottom && p_1_top < b_bottom) c.dir = COLLISION_BOTTOM;
				
				return c;
			} 
		}
	}

	return c;
}

}