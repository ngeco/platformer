#include "animation.h"



SDL_Rect animation_frame(struct entity ent, int frame){
	SDL_Rect r;
	int n = 4;
	int k = 12;
	//printf("frame : %d",frame);
	if(frame < 4){
	//	printf("<4\n");
		SDL_Rect r = {ent.rect.w*frame,0,ent.rect.w,ent.rect.h};
		return r;
	}else if(frame < 8){
	//	printf("<8\n");
		SDL_Rect r = {ent.rect.w*((frame)%n),ent.rect.h,ent.rect.w,ent.rect.h};
		return r;
	}else if(frame < 12){
	//	printf("<12\n");
		SDL_Rect r = {ent.rect.w*((frame)%n),ent.rect.h*2,ent.rect.w,ent.rect.h};
		return r;
	}else{
		SDL_Rect r = {0,0,ent.rect.w,ent.rect.h};
		return r;
	}
}