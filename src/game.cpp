#include "game.h"
#include <math.h>
#include "collision.h"
#include "animation.h"
#include "save.h"
#include "engine.h"
#include <fstream>

enum : int{
	FACE_LEFT,
	FACE_RIGHT
};

enum : int {
	DIR_LEFT,
	DIR_RIGHT
};

struct entity player;
struct entity ents[ents_max];

float jump_velocity = 12.5f; //v1 12.5f
float velocity_y = 0.0f;
float velocity_x = 0.0f;

int direction = DIR_LEFT;

float gravity = .3f;
float terminal_velocity = 12.5f; //v1 10

const float GRAVITY_DEFAULT = 0.3f;
const float	GRAVITY_DIVE = 0.8f;
const float	GRAVITY_SLIDE = 0.2f;
const float	GRAVITY_NULL = 0.0f;

const int dive_frames = 6;
int dive_counter = 0;

const float ACCELERATION_DEFAULT = 0.3f;

float acceleration = 0.3f;
float top_speed = 7.5f; //v1 7.5

float slide_time = 1.0f;

bool grounded = false;
bool on_wall = false;

int wall_time = 0;
int wall_time_max = 60;

bool on_cieling = false;

std::string levels[LEVELS_MAX];
int levels_total = 0;
int current_level;

bool game_paused = false;

enum : int{
	PLAYER_STATE_DIVE,
	PLAYER_STATE_RUN,
	PLAYER_STATE_SLIDE,
	PLAYER_STATE_JUMP
};

int player_state = PLAYER_STATE_JUMP;

float mouse_x = 0.0f;
float mouse_y = 0.0f;
int ent_drag = 0;

int ent_resize = 0;
int ent_selected = 0;

bool stuck_in_ground = false;
int stuck_in_ground_timer = 0;
int stuck_in_ground_timer_max = 60;

bool stuck_in_wall = false;
int stuck_in_wall_timer = 0;
int stuck_in_wall_timer_max = 60;

int stuck_in_ent = 0;

int plat_key[PLAT_TOTAL] = {ENT_TYPE_PLATFORM, 
							ENT_TYPE_PLATFORM_MOVE_X,
							ENT_TYPE_PLATFORM_MOVE_Y};

/*struct cannon{
	int base_index = 0;
	int barrel_index = 0;
};

struct cannon cannons[ents_max];
int cannons_total = 0;*/

struct entity bullets[ents_max];
int bullets_total = 0;
const float bullet_speed = 20.0f;

const int cannon_fire_rate = 45;


static void init_level(int lvl){
	ent_resize = SaveLoad::load_level(ents, levels[lvl]);
	if(ent_resize != 0) ents[ent_resize].hide = true;

	struct entity e;
	e.hide = true;
	for(int i = 0; i < ents_max; i++){
		bullets[i] = e;
	}
	bullets_total = 0;
}

static void init_player(float x, float y){
	ents[0].x = x;
	ents[0].y = y;
	ents[0].type = ENT_TYPE_PLAYER;
	ents[0].rect = {(int)ents[0].x,(int)ents[0].y,32,32};
	ents[0].texture = TEX_PLAYER_RIGHT_SHEET;
	ents[0].total = 1;
	ents[0].frame_rect = animation_frame(ents[0], 0);
}


static void add_bullet(int x, int y, int set_index, int rotation){
	int i = 0;
	while(i < ents_max){
		if(bullets[i].hide == true) break;
		else i++;
	}
	if(i != ents_max){
		struct entity bullet;
		bullet.type = ENT_TYPE_BULLET;
		bullet.texture = TEX_BULLET;
		bullet.x = x + 16;
		bullet.y = y + 16;
		bullet.rect = {x, y, 16, 16};
		SDL_Rect f = {0,0,16,16};
		bullet.frame_rect = f;
		bullet.set_index = set_index;
		//bullet.rotation = rotation;
		float dx = bullet.x - ents[0].x;
		float dy = bullet.y - ents[0].y;
		float r = std::sqrt(dx*dx + dy*dy);
		bullet.velocity_x = -dx/r;
		bullet.velocity_y = -dy/r;
		//bullet.velocity_x = cosf(rotation*180.0f/M_PI);
		//bullet.velocity_y = sinf(rotation*180.0f/M_PI);
		//printf("bullet_velocity (%f, %f) , x, y (%d, %d)\n", bullet.velocity_x, bullet.velocity_y, x, y);
		bullet.hide = false;
		bullets[i] = bullet; 
		//printf("bullet: %d\n", i);
		//bullets_total++;
	}
}

static void update_bullet(int index){

	
	//printf("bullet index: %d hide: %d\n ", index, bullets[index].hide);

	if(bullets[index].time > 300){
		
		bullets[index].hide = true;
		return;
	}

	bullets[index].time++;

	bullets[index].x += bullets[index].velocity_x * bullet_speed;
	bullets[index].y += bullets[index].velocity_y * bullet_speed;
	bullets[index].rect.x = bullets[index].x;
	bullets[index].rect.y = bullets[index].y;

	struct Collision::collision_rect p_0, p_1;
	p_0.x = bullets[index].x;
	p_0.y = bullets[index].y;
	p_0.w = bullets[index].rect.w;
	p_0.h = bullets[index].rect.h;
	

	struct Collision::collision c = Collision::aabb_collision_dir(ents_max+1, p_0,p_0,ENT_TYPE_PLATFORM,ents);

	if(c.dir == COLLISION_NULL) return;

	switch(ents[c.ent].type){
	
	case ENT_TYPE_PLAYER:
		init_level(current_level);
		break;
	case ENT_TYPE_CANNON_BARREL: break;
	case ENT_TYPE_CANNON_BASE: break;

	default:
		bullets[index].hide = true;
		break;
	}

	
}
static void add_cannon_base(SDL_Rect r, int ent_index, int set_index){
	struct entity c_base;
	c_base.type = ENT_TYPE_CANNON_BASE;
	c_base.texture = TEX_CANNON_BASE;
	c_base.x = r.x;
	c_base.y = r.y;
	c_base.rect = r;
	SDL_Rect f = {0,0,32,32};
	c_base.frame_rect = f;
	c_base.set_index = set_index;
	ents[ent_index] = c_base; 
	
	ents[0].total++;
}
static void add_cannon_barrel(SDL_Rect r, int ent_index, int set_index){
	struct entity c_barrel;
	c_barrel.type = ENT_TYPE_CANNON_BARREL;
	c_barrel.texture = TEX_CANNON_BARREL;
	c_barrel.x = r.x;
	c_barrel.y = r.y;
	c_barrel.rect = r;
	SDL_Rect f = {0,0,32,32};
	c_barrel.frame_rect = f;
	c_barrel.set_index = set_index;
	ents[ent_index] = c_barrel; 
	
	ents[0].total++;
}
static void add_cannon(SDL_Rect r){

	int i = ents[0].total;
	if(i + 1 > ents_max){
		return;
	}

	//struct cannon c = {i, i + 1};
	//cannons[cannons_total] = c;


	add_cannon_barrel(r, i, i+1);
	add_cannon_base(r, i+1, i);

	
}

static void update_cannon(int i){
	//point toward player

	float old_rotation = ents[i].rotation;
	float new_rotation = 0.0f;

	float dtheta = -(M_PI/180.0f)*ents[i].rotation;

	float op = ents[0].x - (ents[i].x + ents[i].rect.w/2);
	float adj = ents[0].y - (ents[i].y + ents[i].rect.h/2); 

	float theta = atanf(op/adj);

	if(adj > 0.0f && theta > 0.0f) theta += M_PI;
	if(adj > 0.0f && theta <= 0.0f) theta = M_PI + theta;
	if(theta > 0.0f) theta = -2*M_PI + theta;

	new_rotation = -theta * 180.0f / M_PI;


	float max_rotation = 0.5f;


	dtheta -= theta;

	ents[i].rotation = new_rotation;

	//shoot

	if(ents[i].time > cannon_fire_rate){
		add_bullet(ents[i].x, ents[i].y, i, ents[i].rotation);
		ents[i].time = 0;
	}else{
		ents[i].time++;
	}
}

static void add_resize(float x, float y){
	struct entity e;
	if(ents[0].total >= ents_max) return;

	e.x = x + 8;
	e.y = y + 8;
	SDL_Rect r = {x + 8,y + 8,16,16};
	e.rect = r;
	e.frame_rect = {0,0,16,16};
	e.type = ENT_TYPE_RESIZE;
	e.hide = false;
	e.texture = TEX_RESIZE;

	ents[ents[0].total] = e;
	ent_resize = ents[0].total;
	ents[0].total++;
}
static void set_resize(float x, float y){

	ents[ent_resize].x = x + 8;
	ents[ent_resize].y = y + 8;
	SDL_Rect r = {x+8,y+8,16,16};
	ents[ent_resize].rect = r;
	ents[ent_resize].frame_rect = {0,0,16,16};

	ents[ent_resize].hide = false;

}

static void add_platform(SDL_Rect r){
	if(ents[0].total > ents_max){
		return;
	}
	struct entity plat;
	plat.type = ENT_TYPE_PLATFORM;
	plat.texture = TEX_PLATFORM;
	plat.x = r.x;
	plat.y = r.y;
	plat.rect = r;
	SDL_Rect f = {0,0,32,32};
	plat.frame_rect = f;
	ents[ents[0].total] = plat; 
	
	ents[0].total++;
}

static void clear_ents(){
	struct entity e;
	for(int i = 0; i < ents_max; i++){
		ents[i] = e;
	}
}




/*static void player_gravity(){
	velocity_y -= gravity;

	
}*/

static void move_x(){
	struct Collision::collision_rect p_0, p_1;
	p_0.x = ents[0].x;
	p_0.y = ents[0].y;
	p_0.w = ents[0].rect.w;
	p_0.h = ents[0].rect.h;
	p_1 = p_0;


	p_1.x = (float)(p_1.x + velocity_x);
	if(velocity_x == 0.0f){
		p_1.x = (float)(p_1.x + acceleration);
	}

	struct Collision::collision c = Collision::aabb_collision_dir(0, p_0,p_1,ENT_TYPE_PLATFORM,ents);
	
	if(ents[c.ent].type == ENT_TYPE_GOAL){
		Game::next_level();
		return;
	}else if(ents[c.ent].type == ENT_TYPE_LAVA){
		//SaveLoad::load_level(ents, levels[current_level]);
		init_level(current_level);
		return;
	}

	switch(c.dir){
		

		case COLLISION_RIGHT:
			if(!grounded){
				on_wall = true;
				stuck_in_ent = c.ent;
				if(velocity_y <= 0 && gravity != GRAVITY_NULL) gravity = GRAVITY_SLIDE;

				if(gravity == GRAVITY_DIVE){
					stuck_in_wall = true;
					
					printf("stuck_in_wall\n");
					gravity = GRAVITY_NULL;
					velocity_y = 0.0f;
					stuck_in_wall_timer = 0;
				}
			}

			
			
			velocity_x = 0.0f;
			direction = DIR_LEFT;
			ents[0].x = (ents[c.ent].rect.x + ents[c.ent].rect.w +0.01);
			ents[0].rect.x = (int)ents[0].x - 1;

			break;
		case COLLISION_LEFT:
			if(!grounded && gravity != GRAVITY_NULL) {
				on_wall = true;
				stuck_in_ent = c.ent;
				if(velocity_y <= 0) gravity = GRAVITY_SLIDE;

				if(gravity == GRAVITY_DIVE){
					stuck_in_wall = true;
					printf("stuck_in_wall\n");
					gravity = GRAVITY_NULL;
					velocity_y = 0.0f;
					stuck_in_wall_timer = 0;
				}
			}

			
			
			velocity_x = 0.0f;
			direction = DIR_RIGHT;
			ents[0].x = ents[c.ent].rect.x - ents[0].rect.w - 0.01f;
			ents[0].rect.x = (int)ents[0].x + 1;

			break;
		case COLLISION_BOTTOM:
			break;
		case COLLISION_NULL:
			if(on_wall) gravity = GRAVITY_DEFAULT;
			on_wall = false;
			stuck_in_ent = 0;
			ents[0].x = ents[0].x + velocity_x;
			ents[0].rect.x = (int)ents[0].x;
			break;
		case COLLISION_INSIDE:
			break;

	}

	

}
static void move_y(){
	struct Collision::collision_rect p_0, p_1;
	p_0.x = ents[0].x;
	p_0.y = ents[0].y;
	p_0.w = ents[0].rect.w;
	p_0.h = ents[0].rect.h;
	p_1 = p_0;
	

	if(!grounded) p_1.y = (float)(p_1.y + velocity_y);
	else p_1.y = (float) (p_1.y+gravity);

	struct Collision::collision c = Collision::aabb_collision_dir(0, p_0,p_1,ENT_TYPE_PLATFORM,ents);
	
	if(ents[c.ent].type == ENT_TYPE_GOAL){
		Game::next_level();
		return;
	}else if(ents[c.ent].type == ENT_TYPE_LAVA){
		//SaveLoad::load_level(ents, levels[current_level]);
		init_level(current_level);
		return;
	}

	switch(c.dir){
		
		case COLLISION_TOP:
			grounded = true;
			if(gravity == GRAVITY_DIVE){
				stuck_in_ground = true;
				
				velocity_x = 0.0f;
				stuck_in_ground_timer = 0;
			}
			stuck_in_ent = c.ent;
			gravity = GRAVITY_DEFAULT;
			dive_counter = 0;
			on_wall = false;
			velocity_y = 0.0f;
			ents[0].y = (float)((ents[c.ent].rect.y) - p_1.h -0.01);
			ents[0].rect.y = (int)ents[0].y + 1;
			break;


		case COLLISION_BOTTOM:
			on_cieling = true;
			velocity_y = 0.0f;
			ents[0].y = (ents[c.ent].rect.y + ents[c.ent].rect.h);
			ents[0].rect.y = (int)ents[0].y - 1;
			break;
		case COLLISION_NULL:
			grounded = false;
			ents[0].y = ents[0].y + velocity_y;
			ents[0].rect.y = (int)ents[0].y;
			break;
		case COLLISION_INSIDE:
			break;

	}

}

static void move(){


	if(!stuck_in_ground) move_x();
	if(direction == DIR_LEFT){
		acceleration = -ACCELERATION_DEFAULT;
	}else{
		acceleration = ACCELERATION_DEFAULT;
	}
	if(gravity != GRAVITY_NULL) move_y();

	

}



static void player_jump(float a){
	velocity_y = -jump_velocity;
	if(acceleration < 0) velocity_x = a*top_speed;
	else velocity_x = -a*top_speed;
	grounded = false;
	on_wall = false;
	dive_counter = 0;
	stuck_in_ground = false;
	stuck_in_wall = false;
	stuck_in_wall_timer = 0;
	gravity = GRAVITY_DEFAULT;

}

static void player_turn(){
	//velocity_x = 0.0f;
	acceleration = -acceleration;
	if(direction == DIR_LEFT) direction = DIR_RIGHT;
	else if(direction == DIR_RIGHT) direction = DIR_LEFT;
}

const int run_frame_max = 4;
int run_frame = 0;
const int run_time = 60;
int run_counter = 0;

static void player_animate(){

	run_counter += (int)fabs(velocity_x);

	if(acceleration < 0){
		ents[0].texture = TEX_PLAYER_RIGHT_SHEET;

		if(run_counter > run_time ){
			run_frame = (run_frame + 1) % run_frame_max;
			run_counter = 0;
		}
		//printf("%d\n",run_frame );
		ents[0].frame_rect = animation_frame(ents[0],run_frame);
		if(gravity == GRAVITY_DIVE || stuck_in_ground == true){
			ents[0].frame_rect = animation_frame(ents[0],8);
			//ents[0].texture = TEX_PLAYER_RIGHT_DIVE;
		}else if(velocity_x <= 0.0f && grounded){
			ents[0].frame_rect = animation_frame(ents[0],4);
			//ents[0].texture = TEX_PLAYER_RIGHT_SLIDE;
		}
		if(on_wall){
			ents[0].frame_rect = animation_frame(ents[0],0);
		}else if(!grounded && gravity != GRAVITY_DIVE){
			ents[0].frame_rect = animation_frame(ents[0],0);
		}
		//printf("right : %d\n",ents[0].texture );
	}else{
		ents[0].texture = TEX_PLAYER_LEFT_SHEET;
		if(run_counter > run_time ){
			run_frame = (run_frame + 1) % run_frame_max;
			run_counter = 0;
		}
		//printf("%d\n",run_frame );
		ents[0].frame_rect = animation_frame(ents[0],run_frame);
		if(gravity == GRAVITY_DIVE || stuck_in_ground == true){
			ents[0].frame_rect = animation_frame(ents[0],8);
			//ents[0].texture = TEX_PLAYER_LEFT_DIVE;

		}else if(velocity_x >= 0.0f && grounded){
			ents[0].frame_rect = animation_frame(ents[0],4);
			//ents[0].texture = TEX_PLAYER_LEFT_SLIDE;
		}
		if(on_wall){
			ents[0].frame_rect = animation_frame(ents[0],0);
		}else if(!grounded && gravity != GRAVITY_DIVE){
			ents[0].frame_rect = animation_frame(ents[0],0);
		}

	}
}

static void action(){
	if(grounded){
		if(direction == DIR_LEFT){
			if(velocity_x <= 0.0f) player_jump(1.0f);
			else if(!stuck_in_ground) player_turn();
		}else if(direction == DIR_RIGHT){
			if(velocity_x >= 0.0f) player_jump(1.0f);
			else if(!stuck_in_ground){
				player_turn();
			}
		}
	}else if(on_wall){
		if(stuck_in_wall){
			player_jump(1.0f);
			gravity = GRAVITY_DEFAULT;
			stuck_in_wall = false;
			stuck_in_ent = 0;
		}else if(fabs(velocity_x) < slide_time) player_jump(1.0f);
	}
	if(!grounded && dive_counter >= dive_frames && gravity == GRAVITY_DEFAULT){
		gravity = GRAVITY_DIVE;
		dive_counter = 0;
	}
		
	if(gravity == GRAVITY_DIVE && dive_counter >= dive_frames){
		gravity = GRAVITY_DEFAULT;
		dive_counter = 0;
	}
	if(stuck_in_ground){
		player_jump(1.0f);
		gravity = GRAVITY_DEFAULT;
		stuck_in_ground = false;
		stuck_in_ent = 0;
	}
	
	if(!grounded && !on_wall) stuck_in_ent = 0;
}

static void counters(){
	if(!grounded && dive_counter < dive_frames){
		dive_counter++;
	}
	if(stuck_in_ground){
		if(stuck_in_ground_timer < stuck_in_ground_timer_max){
			stuck_in_ground_timer++;
		}else{
			stuck_in_ground = false;
			stuck_in_ground_timer = 0;
			velocity_x = 0.0f;
			player_turn();
			
		}
	}
	if(stuck_in_wall){
		if(stuck_in_wall_timer < stuck_in_wall_timer_max){
			stuck_in_wall_timer++;
		}else{
			stuck_in_wall = false;
			stuck_in_wall_timer = 0;
			velocity_y = 0.0f;
			gravity = GRAVITY_SLIDE;			
		}
	}
}
static void apply_gravity(){
	//apply gravity when not grounded
	if(!grounded and velocity_y < terminal_velocity) velocity_y += gravity;

	//apply acceleration
	if(fabs(velocity_x) <= top_speed && grounded){
		velocity_x -= acceleration;
		if(fabs(velocity_x) > top_speed) {
			if(velocity_x > 0) velocity_x = top_speed;
			else if(velocity_x < 0) velocity_x = -top_speed;
		}
	}
}

static void update_player(int input, int index){
	apply_gravity();
	move();

	if(input == INPUT_BUTTON) action();

	counters();
	player_animate();
}

namespace Game{

void change_platform_type(int d){
	int type = ents[ent_selected].type;
	if(ent_selected != 0 && 
		(type == ENT_TYPE_PLATFORM
		|| type == ENT_TYPE_PLATFORM_MOVE_X
		|| type == ENT_TYPE_PLATFORM_MOVE_Y
		|| type == ENT_TYPE_LAVA)){
		

		int k = ents[ent_selected].type;

		do{
			k += d;
			if(k < 0) k = ENT_TYPE_TOTAL - 1;
			else k = k % ENT_TYPE_TOTAL;
		}while(k == ENT_TYPE_PLAYER || k == ENT_TYPE_RESIZE || k == ENT_TYPE_GOAL || k == ENT_TYPE_BULLET || k == ENT_TYPE_CANNON_BARREL || k == ENT_TYPE_CANNON_BASE || k == ENT_TYPE_NULL);
		
		ents[ent_selected].type = k;
		printf("type: %d\n",k );
		
		if(k == ENT_TYPE_PLATFORM) ents[ent_selected].texture = TEX_PLATFORM;
		else if(k == ENT_TYPE_PLATFORM_MOVE_X) ents[ent_selected].texture = TEX_PLATFORM_MOVE_X;
		else if(k == ENT_TYPE_PLATFORM_MOVE_Y) ents[ent_selected].texture = TEX_PLATFORM_MOVE_Y;
		else if(k == ENT_TYPE_LAVA) ents[ent_selected].texture = TEX_LAVA;


	}
}

void new_platform(){
	SDL_Rect r = {mouse_x, mouse_y, 32,32};
	add_platform(r);
	printf("%d\n",ents[0].total );
}

void new_cannon(){
	SDL_Rect r = {mouse_x, mouse_y, 32,32};
	add_cannon(r);
}

void new_level(){
	current_level = levels_total;
	clear_ents();
	std::string s1 = "level";
	std::string s2 = std::to_string(levels_total+1);
	levels[current_level] = s1 + s2;
	printf("current_level: %s\n", levels[current_level].c_str());
	init_player(500.0f,500.0f);

}
struct entity *init(){

	Renderer::init("platformer");

	struct entity e;
	for(int i = 0; i<ents_max; i++){
		ents[i] = e;
	}
	levels_total = SaveLoad::load_level_list(levels);

	init_level(0);
	//ent_resize = SaveLoad::load_level(ents, levels[0]);
	//if(ent_resize != 0) ents[ent_resize].hide = true;

	return ents;
}
void next_level(){
	current_level = (current_level + 1) % levels_total;
	/*ent_resize = SaveLoad::load_level(ents, levels[current_level]); 
	if(ent_resize != 0) ents[ent_resize].hide = true;*/
	init_level(current_level);
	printf("player x,y : (%f, %f), %d\n", ents[0].x, ents[0].y, ents[0].hide);
}
void last_level(){
	current_level = (current_level - 1);
	if(current_level < 0) current_level = levels_total - 1;
	/*ent_resize = SaveLoad::load_level(ents, levels[current_level]);
	if(ent_resize != 0) ents[ent_resize].hide = true;*/
	init_level(current_level);
}

void save_current_level(){
	SaveLoad::save_level(ents, levels[current_level]);
	if(current_level == levels_total) levels_total ++;
	printf("total : %d , current_level: %s\n", levels_total, levels[levels_total].c_str());
	SaveLoad::save_level_list(levels, levels_total);
}



void grab(){


	struct Collision::collision_rect mouse = {mouse_x, mouse_y, 1, 1};

	struct Collision::collision c = Collision::aabb_collision_dir(-1, mouse, mouse, ENT_TYPE_PLATFORM, ents);
	if(c.dir != COLLISION_NULL 
		&&	(ents[c.ent].type == ENT_TYPE_PLATFORM
			|| ents[c.ent].type == ENT_TYPE_PLATFORM_MOVE_Y
			|| ents[c.ent].type == ENT_TYPE_PLATFORM_MOVE_X)
			|| ents[c.ent].type == ENT_TYPE_GOAL
			|| ents[c.ent].type == ENT_TYPE_CANNON_BASE
			|| ents[c.ent].type == ENT_TYPE_CANNON_BARREL
			|| ents[c.ent].type == ENT_TYPE_LAVA){

		ent_drag = c.ent;
		ent_selected = c.ent;
		if(ent_resize != 0){
			set_resize(ents[c.ent].x + ents[c.ent].rect.w,ents[c.ent].y + ents[c.ent].rect.h);
		}else{
			add_resize(ents[c.ent].x + ents[c.ent].rect.w,ents[c.ent].y + ents[c.ent].rect.h);
		}
		printf("%d\n",ent_resize );
	}else if(ents[c.ent].type == ENT_TYPE_RESIZE){
		ent_drag = c.ent;
	}else if(c.dir == COLLISION_NULL){
		ent_selected = 0;
		ents[ent_resize].hide = true;
	}

	if(!game_paused){

		ents[ent_resize].hide = true;
	}
}
void drop(){
	ent_drag = 0;
}
void update_mouse(float x, float y){ 

	if(ent_drag != 0){
		

		struct Collision::collision_rect r0;
		struct Collision::collision_rect r1;

		int resize_x = ents[ent_drag].x;
		int resize_y = ents[ent_drag].y;

		if((ents[ent_drag].type == ENT_TYPE_PLATFORM || ents[ent_drag].type == ENT_TYPE_GOAL)&& game_paused){

			r0 = {ents[ent_drag].x, ents[ent_drag].y ,
		 		  ents[ent_drag].rect.w, ents[ent_drag].rect.h};

			ents[ent_drag].x += (x - mouse_x);
			ents[ent_drag].y += (y - mouse_y);
			ents[ent_drag].rect.x = ents[ent_drag].x;
			ents[ent_drag].rect.y = ents[ent_drag].y;

			r1 = {ents[ent_drag].x, ents[ent_drag].y,
		 		  ents[ent_drag].rect.w, ents[ent_drag].rect.h};

		}else if(ents[ent_drag].type == ENT_TYPE_PLATFORM_MOVE_Y && !game_paused){

			r0 = {ents[ent_drag].x, ents[ent_drag].y ,
		 		  ents[ent_drag].rect.w, ents[ent_drag].rect.h};

			//ents[ent_drag].x += (x - mouse_x);
			ents[ent_drag].y += (y - mouse_y);
			//ents[ent_drag].rect.x = ents[ent_drag].x;
			ents[ent_drag].rect.y = ents[ent_drag].y;

			r1 = {ents[ent_drag].x, ents[ent_drag].y,
		 		  ents[ent_drag].rect.w, ents[ent_drag].rect.h};

		 	if(ent_drag == stuck_in_ent){
		 		ents[0].y += (y - mouse_y);
				//ents[ent_drag].rect.x = ents[ent_drag].x;
				ents[0].rect.y = ents[0].y;
		 	}

		}else if(ents[ent_drag].type == ENT_TYPE_PLATFORM_MOVE_X && !game_paused){

			r0 = {ents[ent_drag].x, ents[ent_drag].y ,
		 		  ents[ent_drag].rect.w, ents[ent_drag].rect.h};

			ents[ent_drag].x += (x - mouse_x);
			//ents[ent_drag].y += (y - mouse_y);
			ents[ent_drag].rect.x = ents[ent_drag].x;
			//ents[ent_drag].rect.y = ents[ent_drag].y;

			r1 = {ents[ent_drag].x, ents[ent_drag].y,
		 		  ents[ent_drag].rect.w, ents[ent_drag].rect.h};

		 	if(ent_drag == stuck_in_ent){
		 		ents[0].x += (x - mouse_x);
				//ents[ent_drag].rect.x = ents[ent_drag].x;
				ents[0].rect.x = ents[0].x;
		 	}

		}else if((ents[ent_drag].type == ENT_TYPE_CANNON_BARREL || ents[ent_drag].type == ENT_TYPE_CANNON_BASE) && game_paused){

			r0 = {ents[ent_drag].x, ents[ent_drag].y ,
		 		  ents[ent_drag].rect.w, ents[ent_drag].rect.h};

			ents[ent_drag].x += (x - mouse_x);
			ents[ent_drag].y += (y - mouse_y);
			ents[ent_drag].rect.x = ents[ent_drag].x;
			ents[ent_drag].rect.y = ents[ent_drag].y;

			int s = ents[ent_drag].set_index;

			ents[s].x += (x - mouse_x);
			ents[s].y += (y - mouse_y);
			ents[s].rect.x = ents[s].x;
			ents[s].rect.y = ents[s].y;

			r1 = {ents[ent_drag].x, ents[ent_drag].y,
		 		  ents[ent_drag].rect.w, ents[ent_drag].rect.h};
		 	

		}else if(ents[ent_drag].type == ENT_TYPE_RESIZE){

			resize_x = (int)(x - mouse_x);
			resize_y = (int)(y - mouse_y);
			r0 = {ents[ent_selected].x, ents[ent_selected].y ,
		 		  ents[ent_selected].rect.w, ents[ent_selected].rect.h};

			ents[ent_selected].rect.w = ents[ent_selected].rect.w + (int)(x - mouse_x);
			ents[ent_selected].rect.h = ents[ent_selected].rect.h + (int)(y - mouse_y);

			if(ents[ent_selected].type == ENT_TYPE_CANNON_BASE || ents[ent_selected].type == ENT_TYPE_CANNON_BARREL){
				int s = ents[ent_selected].set_index;
				ents[s].rect.w = ents[s].rect.w + (int)(x - mouse_x);
				ents[s].rect.h = ents[s].rect.h + (int)(y - mouse_y);
			}

			r1 = {ents[ent_selected].x, ents[ent_selected].y,
		 		  ents[ent_selected].rect.w, ents[ent_selected].rect.h};

		 	set_resize(ents[ent_selected].x + ents[ent_selected].rect.w,ents[ent_selected].y + ents[ent_selected].rect.h);

		}


		struct Collision::collision c = Collision::aabb_collision_dir(ent_drag, r0, r1, ENT_TYPE_PLAYER, ents);
		if(c.dir != COLLISION_NULL && ents[c.ent].type == ENT_TYPE_PLAYER){
			switch(c.dir){
				case COLLISION_BOTTOM:
					printf("bottom\n");
					ents[0].x += (x - mouse_x) - 0.1f;
					ents[0].y += (y - mouse_y) - 0.1f;
					ents[0].rect.x = ents[0].x + 1;
					ents[0].rect.y = ents[0].y + 1;
					break;
				case COLLISION_TOP:
					printf("top\n");
					ents[0].y += (y - mouse_y) + 0.1f;
					ents[0].rect.x = ents[0].x;
					ents[0].rect.y = ents[0].y - 1;
					break;
				case COLLISION_LEFT:
					printf("left\n");
					ents[0].x += (x - mouse_x) + 0.1f;
					ents[0].rect.x = ents[0].x - 1;
					break;
				case COLLISION_RIGHT:
					printf("right\n");
					ents[0].x += (x - mouse_x) - 0.1f;
					ents[0].rect.x = ents[0].x + 1;

					break;
			}
			
		}


	}

	mouse_x = x;
	mouse_y = y;

	
}

void new_goal(){
	SDL_Rect r = {mouse_x, mouse_y, 32,32};
	if(ents[0].total > ents_max){
		return;
	}
	struct entity goal;
	goal.type = ENT_TYPE_GOAL;
	goal.texture = TEX_GOAL;
	goal.x = r.x;
	goal.y = r.y;
	goal.rect = r;
	SDL_Rect f = {0,0,32,32};
	goal.frame_rect = f;
	ents[ents[0].total] = goal; 
		
	ents[0].total++;
}

struct entity *update(int input){
	//float y_0 = ents[0].y;

	
	
	if(input != INPUT_PAUSE){

		game_paused = false;

		for(int i = 0; i < ents[0].total; i++){
			switch(ents[i].type){
			case ENT_TYPE_PLAYER:
				update_player(input, i);
				
				break;

			case ENT_TYPE_CANNON_BARREL:
				update_cannon(i);
				break;
			}
		}

		//printf("bullets: %d\n", bullets_total );
		for(int i = 0; i < ents_max; i++){
			if(bullets[i].hide == false) update_bullet(i);
		}

		
		
		
	}else{
		game_paused = true;
	}

	Renderer::render_background();
	Renderer::render_ents(ents);
	Renderer::render_ents(bullets);
	Renderer::render_present();

	return ents;
}
}