#include <stdio.h>
#include <string>

//#include "engine.h"
#include "input.h"
#include "game.h"
#include "global.h"
#include "menu.h"

/*
	enum : int{
		INPUT_NULL,
		INPUT_QUIT,
		INPUT_BUTTON
	};

	struct entity{
		int x = 0;
		int y = 0;
		int type = 0;
		SDL_Rect rect = {0,0,0,0};
		int texture = 0;
	};	
*/

int main(){

	//SDL_Renderer *r = Renderer::init("platformer");

	struct entity *ents = Game::init();

	//struct entity *ents = Menu::init();

	int in = INPUT_NULL;
	bool paused = true;
	int mode = MODE_GAME;

	//SDL_Event e;
	//--------------------------------------//
	//------------MAIN-LOOP-----------------//
	//--------------------------------------//
	while( in != INPUT_QUIT )
	{

		//------------------INPUT---------------//
		in = Input::input(mode);

		if(in == INPUT_PAUSE) paused = !paused;

		//--------------RENDER------------------//

		//Renderer::render(ents);

		//--------------UPDATE---------//
		if(mode == MODE_MENU){
			ents = Menu::update();
		}else if(mode == MODE_GAME){
			if(!paused) ents = Game::update(in);
			else ents = Game::update(INPUT_PAUSE);
		}
		

	}
	//--------------------------------------//
	//---------END-OF-MAIN-LOOP-------------//
	//--------------------------------------//

	Renderer::close();
}